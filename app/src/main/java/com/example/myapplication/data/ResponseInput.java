package com.example.myapplication.data;

import java.util.ArrayList;
import java.util.List;

public class ResponseInput {
    public List<Character> mCharInput;

    public ResponseInput(int charCount) {
        mCharInput = new ArrayList<>(charCount);
    }

    public ResponseInput(List<Character> optionsList) {
        mCharInput = optionsList;
    }
}
