package com.example.myapplication.UI;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.adapters.ResponseAdapter;
import com.example.myapplication.data.ResponseInput;
import com.example.myapplication.databinding.FragmentGameBinding;
import com.example.myapplication.viewmodel.GameViewModel;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GameFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private FragmentGameBinding mBinding;
    private GameViewModel mViewModel;
    private RecyclerView mResponseList;
    private RecyclerView mOptionsList;
    private ImageView mLogoImageView;
    private ResponseAdapter mResponseAdapter;
    private ResponseAdapter mOptionsAdapter;
    //private

    public GameFragment() {
        // Required empty public constructor
    }

    public static GameFragment newInstance() {
        return new GameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(GameViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = FragmentGameBinding.inflate(getLayoutInflater());
        initViews();
        return mBinding.getRoot();
    }

    private void initViews() {
        mResponseList = mBinding.responseGridView;
        mOptionsList = mBinding.optionsGridView;
        mLogoImageView = mBinding.logo;

        mResponseList.setLayoutManager(new GridLayoutManager(getContext(), 6, RecyclerView.HORIZONTAL, false));
        mResponseAdapter = new ResponseAdapter();
        mResponseList.setAdapter(mResponseAdapter);

        mOptionsList.setLayoutManager(new GridLayoutManager(getContext(), 6, RecyclerView.HORIZONTAL, false));
        mOptionsAdapter = new ResponseAdapter();
        mOptionsAdapter.setIsItemClickable(mViewModel.getItemClickListener());
        mViewModel.getNextLogo().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((logo, throwable) -> {
                    mResponseAdapter.setInput(new ResponseInput(logo.getName().length()));
                    mResponseList.invalidate();
                    mOptionsAdapter.setInput(new ResponseInput(mViewModel.getOptionsList(logo.getName())));
                });


    }
}