package com.example.myapplication.model;

class ModelFactory {
    public static final LocalRepo mLocalRepo = new LocalRepo();

    public static LocalRepo getLocalRepo() {
        return mLocalRepo;
    }

}
