package com.example.myapplication.viewmodel;

import android.view.View;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.data.Logo;
import com.example.myapplication.model.AppRepo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;

public class GameViewModel extends ViewModel {

    private int mCurrentScore;
    private List<Logo> mLogoSource;
    private int mCurrentSourceIndex = 0;
    private int mBatchCount = 5;
    char[] mCharArray = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private int mOptionsSize = 16;

    public Single<Logo> getNextLogo() {
        if (mCurrentSourceIndex < mLogoSource.size()) {
            return Single.just(mLogoSource.get(mCurrentSourceIndex++));
        }
        return fetchLogoFromRepo();
    }

    private Single<Logo> fetchLogoFromRepo() {
        return AppRepo.getInstance()
                .getLogos(mCurrentSourceIndex, mBatchCount)
                .flatMap(new Function<List<Logo>, SingleSource<Logo>>() {
                    @Override
                    public SingleSource<Logo> apply(List<Logo> logos) throws Throwable {
                        if (mLogoSource == null) {
                            mLogoSource = logos;
                        } else {
                            mLogoSource.addAll(logos);
                        }
                        return Single.just(mLogoSource.get(mCurrentSourceIndex++));
                    }
                });
    }

    public void incrementScore() {
        mCurrentScore++;
    }

    public View.OnClickListener getItemClickListener() {
        return mClickListener;
    }

    public List<Character> getOptionsList(String name) {
        Random random = new Random();
        List<Character> options = new ArrayList<>(mOptionsSize);
        for (char c : name.toCharArray()) {
            options.add(c);
        }
        int filledCount = name.length();
        while (filledCount < mOptionsSize) {
            int index = random.nextInt(mCharArray.length);
            if (options.get(index) == null) {
                continue;
            }
            options.add(index, mCharArray[index]);
        }
        return options;
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
}
