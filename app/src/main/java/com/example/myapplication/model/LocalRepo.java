package com.example.myapplication.model;

import com.example.myapplication.GameApplication;
import com.example.myapplication.Utils.JsonUtils;
import com.example.myapplication.data.Logo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

class LocalRepo implements DataProvider<Logo> {

    private final String mSourceFile = "logo.txt";
    private List<Logo> mLogos;

    @Override
    public List<Logo> getLogos(int continuationToken, int count) {
        String jsonString = JsonUtils.getJsonFromAssets(mSourceFile, GameApplication.getContext());
        if (mLogos == null) {
            mLogos = readJsonString(jsonString);
        }
        if ((continuationToken + count) > mLogos.size()) {
            return mLogos.subList(continuationToken, mLogos.size());
        } else {
            return mLogos.subList(continuationToken, continuationToken+count);
        }
    }

    private List<Logo> readJsonString(String jsonString) {
        Gson gson = new Gson();
        Type listUserType = new TypeToken<List<Logo>>() { }.getType();
        return gson.fromJson(jsonString, listUserType);
    }
}
