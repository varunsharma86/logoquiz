package com.example.myapplication.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.data.ResponseInput;
import com.example.myapplication.databinding.CharInputBoxBinding;

public class ResponseAdapter extends RecyclerView.Adapter<ResponseAdapter.ViewHolder> {

    private ResponseInput mInput;
    public View.OnClickListener mItemClickListener;

    public void setInput(ResponseInput input) {
        mInput = input;
    }

    public void setIsItemClickable(View.OnClickListener clickListener) {
        mItemClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CharInputBoxBinding binding = CharInputBoxBinding.inflate(LayoutInflater.from(parent.getContext()));
        binding.inputText.setOnClickListener(mItemClickListener);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mInput.mCharInput.get(position) != null) {
            holder.mBinding.inputText.setText(mInput.mCharInput.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if(mInput != null) {
            return mInput.mCharInput.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CharInputBoxBinding mBinding;
        public ViewHolder(@NonNull CharInputBoxBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
