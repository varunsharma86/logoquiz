package com.example.myapplication.model;

import java.util.List;

interface DataProvider<T> {

    List<T> getLogos(int continuationToken, int count);

}
