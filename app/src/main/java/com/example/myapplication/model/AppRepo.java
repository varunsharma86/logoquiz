package com.example.myapplication.model;

import com.example.myapplication.data.Logo;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.rxjava3.core.Single;

public class AppRepo {

    private static final AppRepo INSTANCE = new AppRepo();

    private AppRepo() {
    }

    public static AppRepo getInstance() {
        return INSTANCE;
    }

    public Single<List<Logo>> getLogos(int continuationToken, int count) {
        return Single.fromCallable(new Callable<List<Logo>>() {
            @Override
            public List<Logo> call() throws Exception {
                DataProvider localRepo = ModelFactory.getLocalRepo();
                return localRepo.getLogos(continuationToken, count);
            }
        });
    }

}
